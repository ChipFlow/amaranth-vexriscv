# amaranth-vexriscv

Prebuilt VexRiscv Verilog and amaranth-soc integration as a Python package

This has been [moved to GitHub](https://github.com/ChipFlow/amaranth-vexriscv).